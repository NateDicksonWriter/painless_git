# Painless Git

Welcome to the Painless Git repo! 

## A Note

The full text of painless git is © Nate Dickson. Including submitted changes. You are welcome to download this repo and use a markdown type thing to make yourself a book out of it, although the text is formatted for use with [Leanpub's](https://leanpub.com) Markua system and has some non-standard Markdown in it. You are not permitted to remix, distribute, sell, or use this text for anything else. Someday I will probably re-license this under a Creative Commons license, but today is not that day. I need to convince my wife that this whole "writing technical books" thing isn't a massive waste of time. 

## Happier Stuff

I'm making this public for a couple of reasons: 

1. It demonstrates how to use git in context
2. This makes it really easy for people to submit suggested changes

## How to Submit Fixes
I have the best readers in the world, hands down. If you find mistakes in _Painless Git_ the best way to submit changes is to use BitBucket's issue tracking system. There's a couple of reason for this:

1. I'm actually writing this text in [Scrivener]() and the text files that ar ein this repository are regenerated on every  export from that system. 
1. It's easy to fold the issue tracker into my workflow and give you updates on the progress of your input.

So I'd very much prefer you use this system. However, if you _absolutely must_ use a pull request, either because you want to practice them, or because you're just far more comfortable with them, follow the standard steps:

1. Fork this repository
2. Make your changes
3. Use multiple commits if you have multiple, unrelated changes
4. Create a pull request, explaining what changes you made, and why you think they are the right ones. 

This is a little more difficult for me, because I'll take your changes, import them into Scriv and then mark your PR as closed, even though the text hasn't been directly imported. 

## Thank You in Advance!

Again, I have the world's most amazing readers. Thank you so much, just for reading, just for your support. Just for being _you_.
