# Stashes: Quick, Hide Your Code!

The first question I always get asked when I try to teach people about `git stash` is "why would I ever use this?", so I'm going to save you some time and answer that question before I even talk about what `git stash` does. 
There are two reason you might use `git stash`:

1. You are working on code that isn't ready to be committed yet and need to switch branches.
1. You accidentally started working on a new feature before you switched branches. 

Okay, hold onto those answers while I start explaining `git stash`. 

Git provides a way to squirrel away all your uncommitted changes in a strange little local nether-realm, a small pocket dimension that only exists on your own machine and can't be directly synced with any other. This pocket dimension is referred to as "the stash" and it exactly as furtive as it sounds. The stash (or "Stashes", the nomenclature is a bit up in the air) can't be merged into anything, can't be saved on a server anywhere. Your stash is just for you, and it doesn't operate like any other part of git. Even though it sounds like a bad idea wrapped in a dumb idea, the stash is often incredibly helpful.

A> ## Fun Notes For CS People!
A> 
A> If you like data structures or think that understanding them may help you, read on. If you just want to know how to put code in your stash, you can skip this aside. 
A> 
A> In data structure terms, a git repo is a _directed acyclical graph_, meaning things can only move in one direction, each _node_ (or commit) is linked to other nodes by edges that can only go in one direction, and you can't have loops. Think of what a commit tree looks like and you'll see what I'm talking about. 
A> 
A> The stash uses the same underlying mechanisms, but it _behaves_ like a _stack_. [^stacksareFILO] The "commits" in the stack have nothing to do with one another. They are pushed onto the stash and popped off in a first-in-last-out style. This is because the stash has an entirely different purpose than the repository. The repository is meant to keep code changes in order and available for all time. The stash is a place to quickly store things until you're ready to pop them back out and use them.


[^stacksareFILO]: For anyone who isn't currently taking Data Structures 101: remember that _stacks_ are FILO (first in last out) and _queues_ are FIFO (first in first out). Okay, as you were.

So, now let's look at the two most common uses for the stash.

### "Yeah, Let Me Take a Look."

The first use for a stash is helping other people. The scenario is this: you're working away on a feature when a colleague comes and asks if you can take a look at their code and see if you can figure out why it's not working. You're not quite ready to commit your changes, but you can't switch branches with uncommitted changes. So instead of committing your half-baked code you _stash_ it. Type `git stash` and every local change is carefully stored away and your working directory is returned to a pristine state, as of your last commit. Now you're free to `git checkout` your co-worker's branch and help them with their problem.

Q> ## Why Not Just Commit?
Q> 
Q> I ask this question a lot as well. And it has to do with your team culture. Some teams have a strong culture of never checking in (known) broken code, even in a feature branch. The theory is that any code in the repo should theoretically run. 
Q> 
Q> I don't personally subscribe to this philosophy. I believe you should never (knowingly) break the `test` or `master` branches, because those are shared resources that need to be deployed and we don't want to waste developer time _or_ server time on (known) broken code. 
Q> 
Q> Another reason is that sometimes you really just don't feel like your code is "ready". Like, it's not just _broken_, it's _unfinished_, and you're not comfortable checking that in, making it part of the project's history. I get that. 
Q> 
Q> Whatever your reason, there really _are_ reasons for stashing code instead of committing it. 

### "Awwwww, I forgot to Check Out!"

The other way stashing can save you goes like this:

You just finished a feature, and it's nice and early in the day. You're still brimming with energy, so you grab the next ticket off the backlog and start working on a shiny new feature. You're ready to make your first commit and you realize: you never switched branches. You're still on your old feature branch, or you're on `test`, or some other branch where your new changes just don't belong.

Stash to the rescue! Type `git stash` and your changes are whisked off into the pocket dimension, held in stasis. Then you can create or switch to the _right_ branch for your changes and type `git stash apply`. Git pulls your changes back out of limbo and you're ready to commit them to the branch for your shiny new feature.

## How to Actually Use Git Stash

So we've covered the main commands: 

1. Type `git stash` to add code to the Stash
1. Type `git stash apply` to update your working directory with the code from the stash.

And for the most part that's all you need to know! If you just use those two commands you'll get a lot of milage out of the feature. But we can do more, and it's easy!

I mentioned earlier that the Stash acts like a stack, meaning you can have many bits of code stashed away. If you just type `git stash` you'll get a message like this:

```
Saved working directory and index state WIP on master: 2a3292c Hey I made some changes!
```

Git is being succinct here because it knows you're busy. Here's what it's trying to tell you:

>  "I made a direct copy of where everything was when you were on the **master** branch. This is all the stuff that hasn't been saved yet, but the last commit had a hash of **2a3292c** and the commit message was _Hey I made some changes!_"

Nice, yeah? 

But what if you don't memorize git commit hashes and you want to name your stash yourself? You can totally do that. The command changes to `git stash save "This is my custom commit message."`

So let's play with that for a bit. Create a few pointless files, or make a few changes, and stash them. Once you've stashed a few changes type `git stash list` and see what happens.

Okay, I should have warned you. The list uses `cat` (or something similar) to show you a bunch of text. This is because you could potentially have thousands of stashed changes, and a _pager_ like cat makes that easier to parse. It's all unix-y stuff. Hit `q` to get back out of the list. 

But while you were in there you saw all the stashes, a little peek into your personal pocket dimension. And now you know that you can `apply` any stash you want from that list, Stack or no stack. The list helpfully gives you an index number next to each stash, so you can type  `git stash apply [index number]` to get any stash back. Check `git stash list` again and you'll see that your stash is still there, snug in its little nether realm. 

But that's messy, surely? Leaving old stashes hanging around forever? What if you want them removed when you re-apply them? 

It's just another command. `git stash pop [index number]` will act like a traditional stack and pop the stash off the stack, applying it and deleting it in one fell swoop. If you just don't want a stash any more you can delete it without applying it, `git stash drop [index]` will do the trick. Or leave the index off and just drop the topmost stash on the list. 

So there you go. Git stash. A safe, snug place to hide your not-ready-for-the-public-repo code, and an easy way to move changed-but-not-committed code from one branch to another. All in one little subcommand.